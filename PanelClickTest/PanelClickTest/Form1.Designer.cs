﻿namespace PanelClickTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.picBoxPharmacy = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.picBoxMilknHoney = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.picBox7Eleven = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.btn_800Pharmacy = new System.Windows.Forms.Button();
            this.btn_WMart = new System.Windows.Forms.Button();
            this.btn_WestZone = new System.Windows.Forms.Button();
            this.btn_SunNMoon = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.btn_Karakoy = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxPharmacy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMilknHoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7Eleven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Teal;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(312, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(315, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "ADMIN PANEL TEST";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // picBoxPharmacy
            // 
            this.picBoxPharmacy.BackColor = System.Drawing.Color.WhiteSmoke;
            this.picBoxPharmacy.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBoxPharmacy.BackgroundImage")));
            this.picBoxPharmacy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxPharmacy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBoxPharmacy.InitialImage = ((System.Drawing.Image)(resources.GetObject("picBoxPharmacy.InitialImage")));
            this.picBoxPharmacy.Location = new System.Drawing.Point(40, 113);
            this.picBoxPharmacy.Margin = new System.Windows.Forms.Padding(10);
            this.picBoxPharmacy.Name = "picBoxPharmacy";
            this.picBoxPharmacy.Size = new System.Drawing.Size(125, 125);
            this.picBoxPharmacy.TabIndex = 1;
            this.picBoxPharmacy.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(218, 113);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(125, 125);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(591, 113);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(125, 125);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(40, 296);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(125, 125);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Location = new System.Drawing.Point(218, 296);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(125, 125);
            this.pictureBox5.TabIndex = 5;
            this.pictureBox5.TabStop = false;
            // 
            // picBoxMilknHoney
            // 
            this.picBoxMilknHoney.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBoxMilknHoney.BackgroundImage")));
            this.picBoxMilknHoney.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxMilknHoney.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBoxMilknHoney.Location = new System.Drawing.Point(591, 296);
            this.picBoxMilknHoney.Name = "picBoxMilknHoney";
            this.picBoxMilknHoney.Size = new System.Drawing.Size(125, 125);
            this.picBoxMilknHoney.TabIndex = 6;
            this.picBoxMilknHoney.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Location = new System.Drawing.Point(771, 113);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(125, 125);
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // picBox7Eleven
            // 
            this.picBox7Eleven.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picBox7Eleven.BackgroundImage")));
            this.picBox7Eleven.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBox7Eleven.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox7Eleven.Location = new System.Drawing.Point(771, 296);
            this.picBox7Eleven.Name = "picBox7Eleven";
            this.picBox7Eleven.Size = new System.Drawing.Size(125, 125);
            this.picBox7Eleven.TabIndex = 8;
            this.picBox7Eleven.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox9.BackgroundImage")));
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(403, 113);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(125, 125);
            this.pictureBox9.TabIndex = 9;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox10.Location = new System.Drawing.Point(403, 296);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(125, 125);
            this.pictureBox10.TabIndex = 10;
            this.pictureBox10.TabStop = false;
            // 
            // btn_800Pharmacy
            // 
            this.btn_800Pharmacy.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_800Pharmacy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_800Pharmacy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_800Pharmacy.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_800Pharmacy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_800Pharmacy.Location = new System.Drawing.Point(40, 232);
            this.btn_800Pharmacy.Name = "btn_800Pharmacy";
            this.btn_800Pharmacy.Size = new System.Drawing.Size(125, 32);
            this.btn_800Pharmacy.TabIndex = 22;
            this.btn_800Pharmacy.Text = "800 Pharmacy";
            this.btn_800Pharmacy.UseVisualStyleBackColor = false;
            this.btn_800Pharmacy.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_WMart
            // 
            this.btn_WMart.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_WMart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_WMart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_WMart.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_WMart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_WMart.Location = new System.Drawing.Point(218, 232);
            this.btn_WMart.Name = "btn_WMart";
            this.btn_WMart.Size = new System.Drawing.Size(125, 32);
            this.btn_WMart.TabIndex = 23;
            this.btn_WMart.Text = "WMart";
            this.btn_WMart.UseVisualStyleBackColor = false;
            // 
            // btn_WestZone
            // 
            this.btn_WestZone.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_WestZone.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_WestZone.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_WestZone.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_WestZone.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_WestZone.Location = new System.Drawing.Point(403, 232);
            this.btn_WestZone.Name = "btn_WestZone";
            this.btn_WestZone.Size = new System.Drawing.Size(125, 32);
            this.btn_WestZone.TabIndex = 24;
            this.btn_WestZone.Text = "West Zone";
            this.btn_WestZone.UseVisualStyleBackColor = false;
            // 
            // btn_SunNMoon
            // 
            this.btn_SunNMoon.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_SunNMoon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SunNMoon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_SunNMoon.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SunNMoon.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_SunNMoon.Location = new System.Drawing.Point(591, 232);
            this.btn_SunNMoon.Name = "btn_SunNMoon";
            this.btn_SunNMoon.Size = new System.Drawing.Size(125, 32);
            this.btn_SunNMoon.TabIndex = 25;
            this.btn_SunNMoon.Text = "Sun N Moon";
            this.btn_SunNMoon.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkCyan;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(591, 416);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 32);
            this.button5.TabIndex = 26;
            this.button5.Text = "Milk N Honey";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkCyan;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(403, 416);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(125, 32);
            this.button6.TabIndex = 27;
            this.button6.Text = "Jam E Jam";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkCyan;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(218, 416);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(125, 32);
            this.button7.TabIndex = 28;
            this.button7.Text = "Blue Mart";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkCyan;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Location = new System.Drawing.Point(40, 416);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(125, 32);
            this.button8.TabIndex = 29;
            this.button8.Text = "Quick Buy";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkCyan;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Location = new System.Drawing.Point(771, 416);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(125, 32);
            this.button9.TabIndex = 30;
            this.button9.Text = "7 Eleven";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // btn_Karakoy
            // 
            this.btn_Karakoy.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_Karakoy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Karakoy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Karakoy.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Karakoy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Karakoy.Location = new System.Drawing.Point(771, 232);
            this.btn_Karakoy.Name = "btn_Karakoy";
            this.btn_Karakoy.Size = new System.Drawing.Size(125, 32);
            this.btn_Karakoy.TabIndex = 31;
            this.btn_Karakoy.Text = "Karakoy";
            this.btn_Karakoy.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 57);
            this.panel1.TabIndex = 32;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightYellow;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(941, 472);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Karakoy);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btn_SunNMoon);
            this.Controls.Add(this.btn_WestZone);
            this.Controls.Add(this.btn_WMart);
            this.Controls.Add(this.btn_800Pharmacy);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.picBox7Eleven);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.picBoxMilknHoney);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.picBoxPharmacy);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxPharmacy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMilknHoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7Eleven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picBoxPharmacy;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox picBoxMilknHoney;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox picBox7Eleven;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button btn_800Pharmacy;
        private System.Windows.Forms.Button btn_WMart;
        private System.Windows.Forms.Button btn_WestZone;
        private System.Windows.Forms.Button btn_SunNMoon;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button btn_Karakoy;
        private System.Windows.Forms.Panel panel1;
    }
}

