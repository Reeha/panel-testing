﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using PanelClickTest.Properties;
using System.Drawing.Drawing2D;

namespace PanelClickTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        
        public IWebElement orders_TAB,
           userSearch,
           admin, users,
           searchClick,
           users1,
           logout,
           excelReport,
           closeReport,
           monthlyReport,
           endDate,
           printReport,
           ordersExport,
           withCancelled_Order,
           deliveredOrder,
           cancelledOrder,
           startDate_Order,
           endDate_Order,
           closeOrder,
           startDate,
           exportProducts,
           searchPriority,
           selectCategory,
           filterStock,
           exportAllProducts,
           yesButton,
           filterAvailability,
           exportAllWithCat,
           clickDiv,
           productPriority,
            image,
            products_TAB,
            category_TAB,
            products1_TAB,
            searchCategoryVal,
            clearData,
            subCategoryName,
            search_CAT,
            searchResult,
            promotion1_TAB,
            promotion_TAB,
            element,
            selectSubCategory,
            dashboard;

        
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Begin 800 Pharmacy Click Test ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                driver = new FirefoxDriver();
                LoginPanel();
                ClickFunctionalityTest();
            }
        }

        int count = 0;
        IWebDriver driver;
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void linklbl800Pharamacy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }
        public void WaitForElementVisible_Load(By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(ExpectedConditions.ElementIsVisible(by));
            }
        }
        public void WaitForElementClickable_Load(By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(ExpectedConditions.ElementToBeClickable(by));
                //wait.Until(ExpectedConditions.ElementToBeClickable(by));
            }
        }

        public void ClickElement(string path)
        {
            //bool flag = false;
            count = 10;

            try
            {
                if (!String.IsNullOrEmpty(path))
                {
                    //while (flag == false)
                    while (count != 0)
                    {
                        try
                        {
                            WaitForElementClickable_Load(By.XPath(path), 10);
                            element = driver.FindElement(By.XPath(path));
                            if (element.Displayed && element.Enabled && element.GetAttribute("aria-disabled") == null)
                            {
                                element.Click();
                                count = 0;
                                //flag = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            //flag = false;
                            count -= 1;
                            if (count <= 0)
                            {
                                System.Console.WriteLine(ex.Message);
                                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
                            }
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }

        public void ClickFunctionalityTest()
        {

            try
            {
                WaitForPageLoad();
                //ClickElement(Resources.loginToast);
                ClickElement(Resources.admin);
                WaitForPageLoad();
                //ClickElement(Resources.adminToast);

                OrderTest();
                ProductTest();
                CategoryTest();
                //PromotionTest();
                UsersTest();
                FeedbackTest();
                SuggestedProducts();
                ControlTest();
                ClickElement(Resources.dashboard);
                WaitForPageLoad();
                ClickElement(Resources.logout);
                WaitForPageLoad();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }

        public void OrderTest()
        {
            ClickElement(Resources.ordersTAB);


            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"), 10);
            //ordersExport = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
            //if (ordersExport.Displayed && ordersExport.Enabled && ordersExport.GetAttribute("aria-disabled") == null)
            //{
            //    ordersExport.Click();
            //}
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"), 10);
            //startDate_Order = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"));
            //if (startDate_Order.Displayed && startDate_Order.Enabled && startDate_Order.GetAttribute("aria-disabled") == null)
            //{
            //    startDate_Order.Click();
            //}
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"), 10);
            //endDate_Order = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"));
            //if (endDate_Order.Displayed && endDate_Order.Enabled && endDate_Order.GetAttribute("aria-disabled") == null)
            //{
            //    endDate_Order.Click();
            //}
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"), 10);
            //withCancelled_Order = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"));
            //if (withCancelled_Order.Displayed && withCancelled_Order.Enabled && withCancelled_Order.GetAttribute("aria-disabled") == null)
            //{
            //    withCancelled_Order.Click();
            //}
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"), 10);
            //cancelledOrder = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"));
            //if (cancelledOrder.Displayed && cancelledOrder.Enabled && cancelledOrder.GetAttribute("aria-disabled") == null)
            //{
            //    cancelledOrder.Click();
            //}
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"), 10);
            //deliveredOrder = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"));
            //if (deliveredOrder.Displayed && deliveredOrder.Enabled && deliveredOrder.GetAttribute("aria-disabled") == null)
            //{
            //    deliveredOrder.Click();
            //} 
            //WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/i[1]"), 10);
            //closeOrder = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/i[1]"));
            //if (closeOrder.Displayed && closeOrder.Enabled && closeOrder.GetAttribute("aria-disabled") == null)
            //{
            //    closeOrder.Click();
            //}
            // ClickElement(Resources.dashboard);
        }

        public void CreateMonthlyReport()
        {
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]"), 10);
            monthlyReport = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]"));
            if (monthlyReport.Displayed && monthlyReport.Enabled && monthlyReport.GetAttribute("aria-disabled") == null)
            {
                monthlyReport.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"), 10);
            startDate = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
            if (startDate.Displayed && startDate.Enabled && startDate.GetAttribute("aria-disabled") == null)
            {
                startDate.Click();
            }

            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]"), 10);
            clickDiv = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]"));
            if (clickDiv.Displayed && clickDiv.Enabled && clickDiv.GetAttribute("aria-disabled") == null)
            {
                clickDiv.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"), 10);
            endDate = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/input[1]"));
            if (endDate.Displayed && endDate.Enabled && endDate.GetAttribute("aria-disabled") == null)
            {
                endDate.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]"), 10);
            clickDiv = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]"));
            if (clickDiv.Displayed && clickDiv.Enabled && clickDiv.GetAttribute("aria-disabled") == null)
            {
                clickDiv.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"), 10);
            printReport = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"));
            if (printReport.Displayed && printReport.Enabled && printReport.GetAttribute("aria-disabled") == null)
            {
                printReport.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"), 10);
            excelReport = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[2]"));
            if (excelReport.Displayed && excelReport.Enabled && excelReport.GetAttribute("aria-disabled") == null)
            {
                excelReport.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[3]"), 10);
            closeReport = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[3]"));
            if (closeReport.Displayed && closeReport.Enabled && closeReport.GetAttribute("aria-disabled") == null)
            {
                closeReport.Click();
            }
        }

        public void ProductTest()
        {
            try
            {
                WaitForPageLoad();
                ClickElement(Resources.dashboard);
                WaitForPageLoad();
                //PriorityProducts();
                //ProductsExport();
                GoToProducts();

                WaitForPageLoad();
                ClickElement(Resources.productSearch_ClearData);
                WaitForPageLoad();
                ClickElement(Resources.productSearch_ProductName);
                ClickElement(Resources.productSearch_Barcode);
                ClickElement(Resources.productSearch_Category);
                ClickElement(Resources.productSearch_Div);
                ClickElement(Resources.productSearch_Stock);
                ClickElement(Resources.productSearch_Div);
                ClickElement(Resources.productSearch_Display);
                ClickElement(Resources.productSearch_Div);
                ClickElement(Resources.productSearch_SubmitButton);
                WaitForPageLoad();
                ClickElement(Resources.addNewProduct_Button);
                WaitForPageLoad();
                ClickElement(Resources.addProduct_Category);
                ClickElement(Resources.addProduct_Div);
                ClickElement(Resources.addProduct_ProductName);
                ClickElement(Resources.addProduct_Quantity);
                ClickElement(Resources.addProduct_Barcode);
                ClickElement(Resources.addProduct_Price);
                ClickElement(Resources.addProduct_Description);
                ClickElement(Resources.addProduct_SubTitle);
                ClickElement(Resources.addProduct_SubDescription);
                ClickElement(Resources.addProduct_ImageUpload);
                WaitForPageLoad();
                ClickElement(Resources.addProduct_ImageUploadCancel);
                ClickElement(Resources.addProduct_IsPrescription);
                ClickElement(Resources.addProduct_IsGramBased);
                ClickElement(Resources.addProduct_Stock);
                ClickElement(Resources.addProduct_Display);
                ClickElement(Resources.addProduct_SubmitButton);
                WaitForPageLoad();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }

        public void ProductsExport()
        {
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/button[1]"), 10);
            exportProducts = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/button[1]"));
            if (exportProducts.Displayed && exportProducts.Enabled && exportProducts.GetAttribute("aria-disabled") == null)
            {
                exportProducts.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]"), 10);
            exportAllProducts = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
            if (exportAllProducts.Displayed && exportAllProducts.Enabled && exportAllProducts.GetAttribute("aria-disabled") == null)
            {
                exportAllProducts.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[2]/a[1]"), 10);
            exportAllWithCat = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[2]/a[1]"));
            if (exportAllWithCat.Displayed && exportAllWithCat.Enabled && exportAllWithCat.GetAttribute("aria-disabled") == null)
            {
                exportAllWithCat.Click();
            }



            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ins[1]"), 10);
            filterStock = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ins[1]"));
            if (filterStock.Displayed && filterStock.Enabled && filterStock.GetAttribute("aria-disabled") == null)
            {
                filterStock.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/ins[1]"), 10);
            filterAvailability = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/ins[1]"));
            if (filterAvailability.Displayed && filterAvailability.Enabled && filterAvailability.GetAttribute("aria-disabled") == null)
            {
                filterAvailability.Click();
            }
            WaitForElementVisible_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"), 10);
            yesButton = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]"));
            if (yesButton.Displayed && yesButton.Enabled && yesButton.GetAttribute("aria-disabled") == null)
            {
                yesButton.Click();
            }
            ClickElement(Resources.dashboard);
        }

        public void PriorityProducts()
        {
            GoToProducts();
            WaitForElementClickable_Load(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/i[1]"), 10);
            productPriority = driver.FindElement(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/i[1]"));
            if (productPriority.Displayed && productPriority.Enabled && productPriority.GetAttribute("aria-disabled") == null)
            {
                productPriority.Click();
            }
            WaitForElementClickable_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]"), 10);
            selectCategory = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]"));
            if (selectCategory.Displayed && selectCategory.Enabled && selectCategory.GetAttribute("aria-disabled") == null)
            {
                selectCategory.Click();
            }
            WaitForElementClickable_Load(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/a[1]"), 10);
            searchPriority = driver.FindElement(By.XPath("/html[1]/body[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/a[1]"));
            if (searchPriority.Displayed && searchPriority.Enabled && searchPriority.GetAttribute("aria-disabled") == null)
            {
                searchPriority.Click();
            }
        }

        public void CategoryTest()
        {
            try
            {
                ClickElement(Resources.dashboard);
                GoToCategory();
                WaitForPageLoad();
                ClickElement(Resources.categorySearch_ClearData);
                WaitForPageLoad();
                ClickElement(Resources.categorySearch_CategoryName);
                ClickElement(Resources.categorySearch_SubmitButton);
                WaitForPageLoad();
                ClickElement(Resources.addNewCategory_Button);
                ClickElement(Resources.addCategory_CategoryName);
                ClickElement(Resources.addCategory_InList);
                ClickElement(Resources.addCategory_ImageUpload);
                WaitForPageLoad();
                ClickElement(Resources.addCategory_ImageUploadCancel);
                ClickElement(Resources.addCategory_SubmitButton);
                ClickElement(Resources.dashboard);
                WaitForPageLoad();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }

        public void PromotionTest()
        {
            try
            {
                ClickElement(Resources.dashboard);
                GoToPromotion();
                ClickElement(Resources.promotionSearch_ClearData);
                WaitForPageLoad();
                ClickElement(Resources.promotionSearch_Title);
                ClickElement(Resources.promotionSearch_SubmitButton);
                WaitForPageLoad();
                ClickElement(Resources.addNewPromotion_Button);
                ClickElement(Resources.addPromotion_Title);
                ClickElement(Resources.addPromotion_Description);
                ClickElement(Resources.addPromotion_SubTitle);
                ClickElement(Resources.addPromotion_SubDescription);

                ClickElement(Resources.addPromotion_Type_Product_Percent);
                ClickElement(Resources.addPromotion_ProductPercent_Product);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_ProductPercent_Discount);

                ClickElement(Resources.addPromotion_Type_SubCategory_Percent);
                ClickElement(Resources.addPromotion_SubCatPercent_SubCategory);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_SubCatPercent_Discount);

                ClickElement(Resources.addPromotion_Type_Product_Amount);
                ClickElement(Resources.addPromotion_ProductAmount_Product);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_ProductAmount_Discount);
                ClickElement(Resources.addPromotion_Type_SubCategory_Amount);
                ClickElement(Resources.addPromotion_SubCatAmount_SubCategory);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_SubCatAmount_Discount);
                ClickElement(Resources.addPromotion_Type_Product_Tag);
                ClickElement(Resources.addPromotion_ProductTag_Product);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_ProductTag_NewPrice);
                ClickElement(Resources.addPromotion_Type_MultipleProduct_Tag);
                ClickElement(Resources.addPromotion_MultiProductTag_Product);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_Type_Product_Arrival);
                ClickElement(Resources.addPromotion_ProductsArrival_Product);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addpromotion_Type_SubCategory_Arrival);
                ClickElement(Resources.addPromotion_SubCatArrival_Category);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_Type_GiftItem);
                ClickElement(Resources.addPromotion_GiftItem_MainItem);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_GiftItem_MainItemQuantity);
                ClickElement(Resources.addPromotion_GiftItem_GiftItem);
                ClickElement(Resources.addPromotion_Div);
                ClickElement(Resources.addPromotion_GiftItem_GiftItemQuantity);
                ClickElement(Resources.addPromotion_Type_Banner);
                ClickElement(Resources.addPromotion_StartDate);
                ClickElement(Resources.addPromotion_EndDate);
                ClickElement(Resources.addPromotion_ImageUpload);
                ClickElement(Resources.addPromotion_ImageUploadCancel);
                ClickElement(Resources.addPromotion_SubmitButton);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }

        public void UsersTest()
        {
            ClickElement(Resources.dashboard);
            WaitForPageLoad();
            ClickElement(Resources.usersTAB);
            ClickElement(Resources.usersTAB_users);
            ClickElement(Resources.userSearch_UserName);
            ClickElement(Resources.userSearch_SubmitButton);
            WaitForPageLoad();
        }

        public void FeedbackTest()
        {
            ClickElement(Resources.dashboard);
            WaitForPageLoad();
            ClickElement(Resources.feedbackTAB);
            ClickElement(Resources.feedbackTAB_Feedback);
            ClickElement(Resources.feedbackSearch_Feedback);
            ClickElement(Resources.feedbackSearch_SubmitButton);
            WaitForPageLoad();
        }

        public void SuggestedProducts()
        {
            ClickElement(Resources.dashboard);
            WaitForPageLoad();
            ClickElement(Resources.feedbackTAB);
            ClickElement(Resources.feedbackTAB_SuggestedProducts);
            ClickElement(Resources.suggestedProductSearch_ProductName);
            ClickElement(Resources.suggestedProductSearch_SubmitButton);
            WaitForPageLoad();
        }

        public void ControlTest()
        {
            ClickElement(Resources.dashboard);
            WaitForPageLoad();
            ClickElement(Resources.controlsTAB);
            ClickElement(Resources.controlsTAB_DeliveryStaff);
            ClickElement(Resources.controlsSearch_ClearData);
            WaitForPageLoad();
            ClickElement(Resources.controlsSearch_DeliveryStaffName);
            ClickElement(Resources.controlsSearch_Store);
            ClickElement(Resources.controlSearch_Div);
            ClickElement(Resources.controlsSearch_SearchButton);
            WaitForPageLoad();
            ClickElement(Resources.addNewStaff_Button);
            WaitForPageLoad();
            ClickElement(Resources.addStaff_Branch);
            ClickElement(Resources.addStaff_Div);
            ClickElement(Resources.addStaff_StaffName);
            ClickElement(Resources.addStaff_SurName);
            ClickElement(Resources.addStaff_PhoneNo);
            ClickElement(Resources.addStaff_SubmitButton);
        }
        IWebElement page;
        public void WaitForPageLoad()
        {
            count = 10;
            page = null;
            while (count != 0)
            {
                try
                {
                    if (page != null)
                    {
                        var waitForCurrentPageToStale = new WebDriverWait(driver, TimeSpan.FromSeconds(600));
                        waitForCurrentPageToStale.Until(ExpectedConditions.StalenessOf(page));
                    }

                    var waitForDocumentReady = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                    waitForDocumentReady.Until((wdriver) => (driver as IJavaScriptExecutor).ExecuteScript("return document.readyState").Equals("complete"));

                    page = driver.FindElement(By.TagName("html"));
                    count = 0;
                }
                catch (Exception ex)
                {
                    count -= 1;
                    if (count <= 0)
                    {
                        System.Console.WriteLine(ex.Message);
                        throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
                    }
                }
            }
        }
        public void VisibleElement(string path, string value)
        {
            //bool flag = false;
            count = 10;

            try
            {
                if (!String.IsNullOrEmpty(path))
                {
                    //while (flag == false)
                    while (count != 0)
                    {
                        try
                        {
                            WaitForElementVisible_Load(By.XPath(path), 10);
                            element = driver.FindElement(By.XPath(path));
                            if (element.Displayed && element.Enabled && element.GetAttribute("aria-disabled") == null)
                            {
                                element.SendKeys(value);
                                count = 0;
                                //flag = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            //flag = false;
                            count -= 1;
                            if (count <= 0)
                            {
                                System.Console.WriteLine(ex.Message);
                                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
                            }
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }

        }

        public void LoginPanel()
        {
            try
            {
                driver.Navigate().GoToUrl(Resources.url);
                driver.Manage().Window.Maximize();
                driver.SwitchTo().ActiveElement();
                VisibleElement(Resources.userName, Resources.userNameValue);
                VisibleElement(Resources.password, Resources.passwordValue);
                ClickElement(Resources.loginButton);
                WaitForPageLoad();
                //Assert.That(driver.Url!= Resources.url);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw new Exception("Details:" + ex.Message + "Detailed Exception" + ex.StackTrace);
            }
        }
        //Go to Category selection clicking Navbar
        public void GoToCategory()
        {
            ClickElement(Resources.productsTAB);
            ClickElement(Resources.productsTAB_Category);
        }

        //Go to Promotion selection clicking Navbar
        public void GoToPromotion()
        {
            ClickElement(Resources.promotionsTAB);
            ClickElement(Resources.promotionsTAB_Promotion);
        }

        //Go to Product selection clicking Navbar
        public void GoToProducts()
        {
            ClickElement(Resources.productsTAB);
            ClickElement(Resources.productsTAB_Products);
        }


    }
}
